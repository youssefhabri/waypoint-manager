import { waypointsStore } from '@/store';
import Waypoint, { WaypointType } from '@/models/Waypoint';
import { LatLng } from '@/models/GPS';

import ROS from 'roslib';
export const ros = new ROS.Ros({ url: 'ws://localhost:9000' });

export function subscribeToGPS(name: string) {
  const topic = new ROS.Topic({
    ros,
    name: `/${name}`,
    messageType: 'sensor_msgs/NavSatFix',
  });

  topic.subscribe((message: any) => {
    // tslint:disable-next-line:no-console
    console.log(name);
    if (
      waypointsStore.waypoints.find((wp: Waypoint) => wp.id === name) !==
      undefined
    ) {
      const position: LatLng = {
        lat: message.latitude,
        lng: message.longitude,
      };
      waypointsStore.updatePositionById({ id: name, position });
    } else {
      waypointsStore.addWaypoint(
        new Waypoint(
          name,
          `Waypoint ${name.toUpperCase()}`,
          {
            lat: message.latitude,
            lng: message.longitude,
          },
          WaypointType.Robot,
          name === 'gps1' ? 'blue' : 'red', // TODO remove this
        ),
      );
    }
  });
}
