import Vue from 'vue';
// @ts-ignore
import Sortable from 'vue-sortable';
// @ts-ignore
import VueNativeWS from 'vue-native-websocket';
import './plugins/element';
import './plugins/google-map';

import App from './App.vue';
import router from './router';
import { store } from './store';

import { Titlebar, Color } from 'custom-electron-titlebar';

const _ = new Titlebar({
  backgroundColor: Color.fromHex('#ECECEC'),
  menu: null,
  titleHorizontalAlignment: 'left',
});

Vue.use(Sortable);

Vue.use(VueNativeWS, 'ws://localhost:9000');

Vue.config.productionTip = false;

new Vue({
  router,
  store,
  render: (h) => h(App),
}).$mount('#app');
