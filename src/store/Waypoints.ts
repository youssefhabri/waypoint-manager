import {
  Module,
  VuexModule,
  getter,
  action,
  mutation,
} from 'vuex-class-component';
import Waypoint from '@/models/Waypoint';
import {LatLng} from '@/models/GPS';

@Module()
export class WaypointsStore extends VuexModule {

  @getter
  public get waypoints(): Waypoint[] {
    return this._waypoints;
  }
  private _waypoints: Waypoint[] = [];

  @action()
  public async addWaypoint(waypoint: Waypoint): Promise<void> {
    this._addWaypoint(waypoint);
  }

  @action()
  public async updateWaypoints(waypoints: Waypoint[]): Promise<void> {
    this._updateWaypoints(waypoints);
  }

  @action()
  public async updatePositionById(payload: { id: string, position: LatLng }): Promise<void> {
    this._updatePositionById(payload);
  }

  @mutation
  protected _addWaypoint(waypoint: Waypoint) {
    this._waypoints.push(waypoint);
  }

  @mutation
  protected _updateWaypoints(waypoints: Waypoint[]) {
    this._waypoints = waypoints;
  }

  @mutation
  protected _updatePositionById(payload: { id: string, position: LatLng }) {
    this._waypoints.forEach((wp: Waypoint, idx) => {
      if (wp.id === payload.id) {
        this._waypoints[idx].position = payload.position;
      }
    });
  }
}

export const waypointsModule = WaypointsStore.ExtractVuexModule(WaypointsStore);
