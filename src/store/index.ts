import Vue from 'vue';
import Vuex from 'vuex';

import { waypointsModule, WaypointsStore } from './Waypoints';

Vue.use(Vuex);

export const store = new Vuex.Store({
  state: {},
  modules: {
    waypointsModule,
  },
});

export const waypointsStore = WaypointsStore.CreateProxy(store, WaypointsStore);
