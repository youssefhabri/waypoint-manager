import { LatLng } from './GPS';

export enum WaypointType {
  Home,
  Normal,
  Robot,
}

export default class Waypoint {
  constructor(
    public id: string,
    public label: string,
    public position: LatLng,
    public type: WaypointType = WaypointType.Normal,
    public icon: string = 'red',
  ) {}

  get home(): boolean {
    return this.type === WaypointType.Home;
  }

  set home(_value: boolean) {
    this.type = WaypointType.Home;
  }

  get isRobot(): boolean {
    return this.type === WaypointType.Robot;
  }

  get latitude(): number {
    return this.position.lat;
  }

  set latitude(latitude: number) {
    this.position.lat = latitude;
  }

  get longitude(): number {
    return this.position.lng;
  }

  set longitude(latitude: number) {
    this.position.lng = latitude;
  }
}
